*** Settings ***
Suite Setup       Login Application
Resource          ../Keywords/Generic.robot
Resource          ../Keywords/E2E_KEYWORDS.robot

*** Test Cases ***
CUSTOMER SHOULD BE ABLE TO COMPOSE PREMIUM IN START PAGE
    Comment    GIVEN CUSTOMER HAS VERIFIED ORGANIZATION INFORMATION
    Comment    WHEN HE COMPOSE THE DETAILS FOR PREMIUM CALCULATION
    Comment    THEN ON SUBMIT PREMIUM DETAILS PAGE SHOULD BE DISPLAYED

CUSTOMER SHOULD BE ABLE TO REVIEW THE PREMIUM CALCULATION PAGE
    [Setup]    Customer is in Premium Calculation Page
    GIVEN CUSTOMER REVIEW THE CALCULATION
    WHEN HE SUBMIT THE PREMIUM CALCULATION
    THEN ADDITIONAL INFORMATION PAGE IS DISPLAYED

CUSTOMER SHOULD BE ABLE TO FILL ADDITIONAL INFORMATION
    [Setup]    Customer is in Additional Information Page
    GIVEN CUSTOMER FILLS THE DETAILS
    WHEN HE CLICKS THE NEXT BUTTON
    THEN SUMMARY PAGE SHOULD BE DISPLAYED
    AND CONFIRMS THE SUMMARY PAGE
