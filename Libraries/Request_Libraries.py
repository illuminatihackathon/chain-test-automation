import requests
import collections
import json
import os
import psutil
from requests.auth import HTTPBasicAuth
from requests.auth import HTTPProxyAuth
from datetime import date
import ibm_db

class Foo:
    def __init__(self):
        self._url = ""
        self._sess = requests.Session()
        self._sess.verify = False
     
    def post_premium_calculation_euler(self,arg,arg1,arg2):
        url="https://eh-krediet-gvc-chain-internal.apps.pcf-t02-we.rabobank.nl/api/PremiumCalculations"
        customer=str(arg)
        amount=str(arg1)
        business=str(arg2)
        self._url = url
        today=date.today()
        today_date=str(today)+'T00:00:00Z'
        next_year=date(today.year+1,today.month,today.day)
        next_year_date=str(next_year)+'T00:00:00Z'
        address=Foo.get_db_details_address(self,customer)
        amount=amount.split(' ')
        amount=amount[len(amount)-1].replace('.','')
        business=business.split('_')
        business=business[len(business)-1]
        post_json='{"AL_VIEWCOD": "78001","AL_DATACAT": "30B","PP_BRANCHE": "070","PP_MYAAND": "E087","PP_PRDTOMS": "Omzet Garant","PP_PRODUCC": "K","KR_CODE": "8003","KR_INGDAT": "'+today_date+'","OG_BEDHOLD": "J","PP_INGDAT": "'+today_date+'","PP_BETTERM": 1,"PP_BETWIJZ": "M","PP_INCWIJZ": "I","COLO_LOKALEBANK": "N","COLO_ANONIEM": "N","OG_ANAAM": "'+customer+'","OG_PSTCD": "'+str(address[2])+'","OG_VSTRAAT": "'+str(address[0])+'","OG_VHUISNR": '+str(address[1])+',"OG_VPLTS": "'+str(address[3])+'","OG_VLAND": "NL","OG_NRHNDRG": "'+str(address[4])+'","AL_FUNCTIE": "01","COLO_OMZAKKHLP": "De verwachte jaaromzet van alle bedrijfsactiviteiten uit leveringen aan zakelijke klanten die gevestigd zijn in de Benelux en Duitsland. Omzet uit leveringen aan particulieren, overheidsinstellingen en eigen bedrijven reken je niet mee.","COLO_HOEDANHLP": "Kies de activiteit waarmee je de meeste omzet behaald uit leveringen aan zakelijke klanten. Let op: Als je gegevens niet kloppen, neem dan contact met ons op. Je bent in dit geval misschien niet verzekerd of niet goed verzekerd. We helpen je graag verder.","PP_EXPDAT": "'+next_year_date+'","PP_DTMVERZ": "'+today_date+'","PP_INGDATO": "'+today_date+'","KR_OMZAKK": "'+amount+'","OG_HOEDAN": "'+business+'"}'
        header={"Content-Type":"application/json","SubscriptionKey":"gn8CSvSWfwHBbyJWiaR5vYZxzZwmXxhj"}
        post_Premium_Calc_Response=self._sess.post(self._url,data=post_json,headers=header,verify=False)
        json_data_response=post_Premium_Calc_Response.json()
        #return json_data_response
        return str(format(json_data_response["PP_BTP"],'.2f')).replace('.',','),str(format(json_data_response["PP_TASS"],'.2f')).replace('.',','),str(format(json_data_response["PP_TTOT"],'.2f')).replace('.',',')
    
    def post_premium_calculation_chubb(self,arg,arg1,arg2,insuredAmt):
        url="https://chubb-cyber-gvc-chain-internal.apps.pcf-t02-we.rabobank.nl/api/PremiumCalculations"
        customer=str(arg)
        annualTurnover=str(arg1)
        business=str(arg2)
        self._url = url
        today=date.today()
        today_date=str(today)+'T00:00:00.000Z'
        next_year=date(today.year+1,today.month,today.day)
        next_year_date=str(next_year)+'T00:00:00.000Z'
        address=Foo.get_db_details_address(self,customer)
        business=business.split('_')
        business=business[len(business)-1]
        insuredAmt=str(insuredAmt)
        insuredAmt=insuredAmt.split(' ')
        insuredAmt=insuredAmt[len(insuredAmt)-1].replace('.','')
        post_json='{"AL_VIEWCOD":"77001","AL_DATACAT":"31","PP_MYAAND":"C505","PP_PRDTOMS":"Cyber ERM","PP_PRODUCC":"00401","OG_BEDHOLD":"N","PD_CODE":"5420","PD_MYCODE":"5421","PP_INGDAT":"2019-12-03T00:00:00.000Z","PP_BETTERM":1,"PP_BETWIJZ":"M","PP_INCWIJZ":"I","COLO_LOKALEBANK":"J","COLO_ANONIEM":"N","OG_ANAAM":"'+customer+'","OG_PSTCD":"'+str(address[2])+'","OG_VSTRAAT":"'+str(address[0])+'","OG_VHUISNR":'+str(address[1])+',"OG_VPLTS":"'+str(address[3])+'","OG_VLAND":"NL","AL_FUNCTIE":"01","OG_OMZET":'+annualTurnover+',"COLO_HOEDANHLP":"Kies de activiteit waarmee je de meeste omzet behaalt. Let op: Als je gegevens niet kloppen, neem dan contact met ons op. Je bent in dit geval misschien niet verzekerd of niet goed verzekerd. We helpen je graag verder.","PD_INGDAT":"'+today_date+'","PD_EXPDAT":"'+next_year_date+'","PP_EXPDAT":"'+next_year_date+'","PP_DTMVERZ":"'+today_date+'","PP_INGDATO":"'+today_date+'","OG_HOEDAN":"'+business+'","PD_MVZCRED":"N","PD_VERZSOM":"'+insuredAmt+'"}'
        header={"Content-Type":"application/json","SubscriptionKey":"gn8CSvSWfwHBbyJWiaR5vYZxzZwmXxhj"}
        post_Premium_Calc_Response=self._sess.post(self._url,data=post_json,headers=header,verify=False)
        json_data_response=post_Premium_Calc_Response.json()
        #return json_data_response
        return str(format(json_data_response["PP_BTP"],'.2f')).replace('.',','),str(format(json_data_response["PP_TASS"],'.2f')).replace('.',','),str(format(json_data_response["PP_TTOT"],'.2f')).replace('.',',')
        
    def get_db_details_address(self,customer):
        customer=str(customer)
        sql = "SELECT STRAAT,HUISNUMMER,POSTCODE,PLAATS,(SELECT X_COMP_REG_NUM FROM SIEBEL.S_ORG_EXT WHERE NAME_1 LIKE '"+customer+"') AS KVK FROM SIEBEL.ACCOUNTADRES  WHERE KLANTID IN (SELECT NAME FROM SIEBEL.S_ORG_EXT WHERE NAME_1 LIKE '"+customer+"')"
        conn = ibm_db.connect("DATABASE=DBAE;HOSTNAME=AKS.ABCD.RABOBANK.NL;PORT=12446;PROTOCOL=TCPIP;UID=JAISWAS;PWD=Sam07pam;", "", "")
        stmt = ibm_db.exec_immediate(conn, sql)
        result = ibm_db.fetch_both(stmt)
        return result[0],result[1],result[2],result[3],result[4]

def get_premium_calc_service_response_euler(customer,amount,business):    
    result=Foo().post_premium_calculation_euler(customer,amount,business)
    return result
    
def get_premium_calc_service_response_chubb(customer,amount,business,insuredAmt):    
    result=Foo().post_premium_calculation_chubb(customer,amount,business,insuredAmt)
    return result
    
    
#print (get_premium_calc_service_response_euler('Brint ORGa Rho BV','2.000.001 t/m 2.500.000','OG_HOEDAN_477300'))
#print (get_premium_calc_service_response_chubb('Brint ORGa Rho BV','50000','OG_HOEDAN_477300','€ 100.000'))

