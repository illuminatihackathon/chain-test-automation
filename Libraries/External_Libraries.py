import ibm_db
from robot.libraries.BuiltIn import BuiltIn
import pyautogui
import time
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

def get_status_from_db(arg):
    arg=str(arg)
    conn = ibm_db.connect("DATABASE=DBAE;HOSTNAME=AKS.ABCD.RABOBANK.NL;PORT=12446;PROTOCOL=TCPIP;UID=JAISWAS;PWD=Sam07pam;", "", "")
    sql = "select o.row_id, o.integration_id, ol.X_UNIT_OF_WORK_ID, ol.row_id orderline, ol.integration_id, org.X_SHORT_NAME as Organisatie, bu.name, ol.action_cd, ol.x_sub_action_cd, ol.status_cd, ol.x_asset_num as Polisnummer, ol.created as Aangemaakt, ol.last_upd as Gewijzigd, org.name as org_id, prod.name, ol.X_ORIGIN_CHANNEL, o.DESC_TEXT, o.status_cd as order, o.row_id, org.x_part_key, ol.x_user_status_cd, ol.x_substatus_cd, ol.asset_integ_id, substr(ol.line_note,1,480) as Notites, ol.desc_text from SIEBEL.S_ORDER o join SIEBEL.S_BU bank on bank.row_id = o.bu_id join SIEBEL.S_ORDER_ITEM ol on ol.order_id = o.row_id JOIN SIEBEL.S_PROD_INT prod ON prod.ROW_ID=ol.PROD_ID join SIEBEL.S_ORG_EXT org on org.row_id = o.accnt_id join SIEBEL.S_BU bu on bu.row_id = org.bu_id where 1=1 and ol.prod_id in ('1-12QY22Z', '1-12QY22T') and ol.status_cd not in ('Geannuleerd') and ol.LAST_UPD  > (CURRENT TIMESTAMP - 13 DAY) order by ol.LAST_UPD  desc FETCH FIRST ROW ONLY"
    stmt = ibm_db.exec_immediate(conn, sql)
    result = ibm_db.fetch_both(stmt)
    if arg=='status':
        return result[9]
    elif arg=='policy':
        return result[10]
    elif arg=='org_id':
        return result[13]
    #count=1
    #colid=5
    #if result != False:
        #while result != False:
        #print (str(count)+" ROW details are below :")
        #print ("The ORIGIN is : "+str(result["ORIGIN"]))
        #print ("The start date is : "+str(result[colid]))
        #result = ibm_db.fetch_both(stmt)
        #print (result)
        #count=count+1
    #else:
        #print ("Empty result")
    
    
#print (get_status_from_db('status'))

def get_db_details_address(customer):
    customer=str(customer)
    sql = "SELECT STRAAT,HUISNUMMER,POSTCODE,PLAATS,(SELECT X_COMP_REG_NUM FROM SIEBEL.S_ORG_EXT WHERE NAME_1 LIKE '"+customer+"') AS KVK FROM SIEBEL.ACCOUNTADRES  WHERE KLANTID IN (SELECT NAME FROM SIEBEL.S_ORG_EXT WHERE NAME_1 LIKE '"+customer+"')"
    conn = ibm_db.connect("DATABASE=DBAE;HOSTNAME=AKS.ABCD.RABOBANK.NL;PORT=12446;PROTOCOL=TCPIP;UID=JAISWAS;PWD=Sam07pam;", "", "")
    stmt = ibm_db.exec_immediate(conn, sql)
    result = ibm_db.fetch_both(stmt)
    return result[0],result[1],result[2],result[3],result[4]
    
print (get_db_details_address('Brint ORGa Pi'))

def input_keyboard_keys():
    time.sleep(5)
    pyautogui.press('esc')

def scroll_pyautogui(arg):
    time.sleep(2)
    arg=int(arg)
    print (arg)
    pyautogui.scroll(arg)
    
#scroll_pyautogui('-10')
    
def get_login_details(customer):
    customer=str(customer)
    loginDetails={
    "Brint ORGa Tau" : ["645444545","1234"],
    "Brint ORGa Tau BV" : ["643031146","6914"],
    "Brint ORGa Rho BV" : ["643021663","3564"],
    "Brint ORGa Pi BV" : ["643016600","6464"],
    "Brint ORGa Eta BV" : ["642982279","5574"],
    "Brint ORGa Rho" : ["645442275","7554"],
    "Brint ORGa Pi" : ["645438081","1574"]}
    list=loginDetails[customer]
    return list[0],list[1]
    
#print (get_login_details('Brint ORGa Tau'))

def get_screen():
    scrw, scrh = pyautogui.size()
    currMouseX, currMouseY = pyautogui.position()
    print (scrw)
    print (scrh)
    print (currMouseX)
    print (currMouseY)
    return scrw, scrh, currMouseX, currMouseY
    
def containString(string,arg):
	arg=str(arg)
	status="False"
	if arg in string:
		status="True"
	return status
    
#print (containString('Automation Framework.Chain Test Suite.SIEBEL-Kredietverzekering_Euler_Hermes_Test_Cases','SIEBEL'))

