*** Settings ***
Library           Selenium2Library
Library           OperatingSystem
Library           Collections
Library           Process
Library           String
Resource          ../Test_Objects/SIEBEL.robot
Library           ../Libraries/External_Libraries.py
Library           ../Libraries/Javascript_Libraries.py
Resource          ../Test_Data/TEST_DATA.robot

*** Keywords ***
Launch Siebel
    Log To Console    Test Data Browser = ${browser}
    Log To Console    Test Data CustomerId = ${customer}
    ${statusChubb}=    containString    ${SUITE NAME}    Chubb
    Log To Console    Is it a Chubb Test Cases = ${statusChubb}
    ${statusEuler}=    containString    ${SUITE NAME}    Euler
    Log To Console    Is it a Euler Test Cases = ${statusEuler}
    ${ins}=    Run Keyword If    '${statusChubb}'=='True' and '${statusEuler}'=='False'    Set Variable    Chubb
    ...    ELSE IF    '${statusChubb}'=='False' and '${statusEuler}'=='True'    Set Variable    Euler
    Log To Console    Test Data Insurance = ${ins}
    Log To Console    Test Data IBAN = ${iban}
    Log To Console    Test Data Email = ${email}
    Set Global Variable    ${browser}
    Set Global Variable    ${customer}
    Set Global Variable    ${iban}
    Set Global Variable    ${email}
    ${insurance}=    Run Keyword If    '${ins}' == 'Euler'    Set Variable    Euler Hermes - Krediet (ook online)
    ...    ELSE IF    '${ins}' == 'Chubb'    Set Variable    Chubb - Cyberrisico (ook online)
    Set Global Variable    ${insurance}
    Set Global Variable    ${ins}
    Open Browser    http://ota-autorisatie-raboweblogin.rabobank.nl/cgi-bin/testloginLB.exe?testuid=3033Allroles&testbankcode=3033    ${browser}
    Maximize Browser Window
    Sleep    1s
    Go To    https://siebel-chain3.rabobank.nl/siebel/app/fins/nld
    Wait Until Page Contains    teamacties    60s

Highlight Object
    [Arguments]    ${objecToBeHighlighted}    ${screenshotName}
    Execute Javascript    ${objecToBeHighlighted}.style.border="solid #FF0000"
    Capture Page Screenshot    ${screenshotName}.png
    Sleep    1s
    Execute Javascript    ${objecToBeHighlighted}.style.border=""

Login Application
    Log To Console    Test Data Browser = ${browser}
    ${customer_login_details}=    get_login_details    ${customer}
    Log to Console    ${\n}Cutomer details : ${customer_login_details}
    Log To Console    Test Data CustomerId = ${customer_login_details}[0]
    Log To Console    Test Data Pin = ${customer_login_details}[1]
    ${customerId}=    Set Variable    ${customer_login_details}[0]
    ${pin}=    Set Variable    ${customer_login_details}[1]
    Open Browser    https://bankieren-acpt3.rabobank.nl/online/nl/qsl_debitcardlogon.do    ${browser}
    Maximize Browser Window
    ${status}=    Run Keyword and Return Status    Wait Until Page Contains    probleem    60s
    Run Keyword If    '${status}'=='True'    Execute Javascript    window.document.getElementById("rass-action-proceed").click();
    Go To    https://bankieren-acpt3.rabobank.nl/online/nl/qsl_debitcardlogon.do
    Sleep    1s
    ${status}=    Run Keyword And Return Status    Wait Until Page Contains    Bankpas    60s
    Run Keyword If    '${status}'=='False'    Go To    https://bankieren-acpt3.rabobank.nl/online/nl/qsl_debitcardlogon.do
    Wait Until Page Contains    Bank card    60s
    Input Text    id=rass-data-reknr    ${customerId}
    Sleep    1s
    Input Text    id=rass-data-pasnr    ${pin}
    Sleep    1s
    ${LoginButtonColor}=    Execute Javascript    return window.getComputedStyle(document.getElementById("rass-action-login")).backgroundColor
    Run Keyword If    '${LoginButtonColor}'=='rgb(255, 102, 0)'    Run Keywords    Log To Console    The Login Button is Enabled Now
    ...    AND    Get Gencode For Login    ${customerId}    ${pin}
    Run Keyword Unless    '${LoginButtonColor}'=='rgb(255, 102, 0)'    Run Keywords    Log To Console    The Login Button is NOT Enabled
    ...    AND    Fail    LogIn ID & Pin is incorrect
    Go To    https://bankieren-acpt3.rabobank.nl/online/nl/qsl_debitcardlogon.do
    Sleep    1s
    Wait Until Page Contains    Bank card    60s
    Sleep    1s
    Input Text    id=rass-data-reknr    ${customerId}
    Sleep    1s
    Input Text    id=rass-data-pasnr    ${pin}
    Sleep    1s
    Input Text    id=rass-data-inlogcode    ${gencode}
    Run Keywords    Click Button    id=rass-action-login
    ...    AND    Sleep    5s
    ...    AND    input_keyboard_keys
    ...    AND    input_keyboard_keys
    Wait Until Page Contains    Mijn overzicht    60s
    Log To Console    Dashboard Page is displayed
    Sleep    1s
    ${statusChubb}=    containString    ${SUITE NAME}    Chubb
    Log To Console    Is it a Chubb Test Cases = ${statusChubb}
    ${statusEuler}=    containString    ${SUITE NAME}    Euler
    Run Keyword If    '${statusChubb}'=='True' and '${statusEuler}'=='False'    Go To    https://bankieren-acpt3.rabobank.nl/online/nl/verzekeren/verzekering-toevoegen/chubb-cyberverzekering-toevoegen
    ...    ELSE IF    '${statusChubb}'=='False' and '${statusEuler}'=='True'    Go To    https://bankieren-acpt3.rabobank.nl/online/nl/verzekeren/verzekering-toevoegen/eh-kredietverzekering-toevoegen
    Sleep    1s
    Run Keyword    input_keyboard_keys
    Wait Until Page Contains    Premie berekenen en aanvragen    60s
    Log To Console    Start Page is displayed

Get Gencode For Login
    [Arguments]    ${customerId}    ${pin}
    Sleep    1s
    Go To    https://bankieren-acpt3.rabobank.nl/gencode/
    Sleep    1s
    Input Text    id=accountNumber    ${customerId}
    Input Text    id=cardNumber    ${pin}
    Sleep    1s
    Execute Javascript    window.document.getElementsByClassName("btn")[9].click();
    Sleep    1s
    ${gencode}=    Execute Javascript    return window.document.getElementById("result-panel").childNodes[6].innerText
    Log To Console    Generated Code = ${gencode}
    Set Global Variable    ${gencode}
    [Return]    ${gencode}
