*** Settings ***
Library           Selenium2Library
Library           OperatingSystem
Library           Collections
Library           Process
Library           String
Resource          ../Test_Objects/ADDITIONAL_INFORMATION.robot
Library           ../Libraries/External_Libraries.py
Library           ../Libraries/Javascript_Libraries.py
Resource          Generic.robot
Resource          TestSetup.robot

*** Keywords ***
CUSTOMER FILLS THE DETAILS
    Input Text    id=OG_IBAN-input    ${iban}
    Sleep    1s
    Input Text    id=OG_EMAIL-input    ${email}
    Sleep    1s
    Log to Console    IBAN & EmailId has been entered
    Run Keyword If    '${ins}'=='Euler'    CLICK RADIO BUTTON FOR EULER    ELSE IF    '${ins}'=='Chubb'    CLICK RADIO BUTTON FOR CHUBB

CLICK RADIO BUTTON FOR EULER
    Sleep    1s
    Execute Javascript    ${COLOKPObj}
    Sleep    1s
    Execute Javascript    ${RIFINPROBObj}
    Sleep    1s
    Execute Javascript    ${RIOGBObj}
    Sleep    1s
    Execute Javascript    ${RISTRAFRVObj}
    Sleep    1s
    Execute Javascript    ${RIELVObj}
    Sleep    1s
    Log to Console    All the Radio Boxes for Euler has been selected

CLICK RADIO BUTTON FOR CHUBB
    Sleep    1s
    Execute Javascript    ${COLOSLOT1NObj}
    Sleep    1s
    Execute Javascript    ${COLOSLOT2NObj}
    Sleep    1s
    Execute Javascript    ${COLOSLOT3NObj}
    Sleep    1s
    Execute Javascript    ${COLOSLOT4NObj}
    Sleep    1s
    Execute Javascript    ${COLOSLOT5NObj}
    Sleep    1s
    Execute Javascript    ${COLOSLOT6NObj}
    Sleep    1s
    Execute Javascript    ${COLOSLOT7NObj}
    Sleep    1s
    Execute Javascript    ${COLOSLOT8NObj}
    Sleep    1s
    Execute Javascript    ${RISCH3JRNObj}
    Sleep    1s
    Log to Console    All the Radio Boxes for Chubb has been selected

HE CLICKS THE NEXT BUTTON
    Sleep    1s
    Execute Javascript    ${NextButtonObj}.click();
    Log to Console    The Next Button Has been Clicked

SUMMARY PAGE SHOULD BE DISPLAYED
    Sleep    1s
    Wait Until Page Contains    Nog even controleren    60s
    Log to Console    The Summary Page has been displayed

CONFIRMS THE SUMMARY PAGE
    Sleep    1s
    Execute Javascript    ${SubmitButtonObj}.click();
    Log to Console    The Submit button on summery page has been clicked
    ANB SHOULD PROCESS THE REQUEST

ANB SHOULD PROCESS THE REQUEST
    Sleep    1s
    Wait Until Page Contains    Ondertekenaars    60s
    ${status}=    get_status_from_db    status
    Run Keyword If    '${status}'=='Gevalideerd'    Log To Console    The Order status is 'Gevalideerd'
    Sleep    1s
    Execute Javascript    window.document.getElementById("nextButton").click();
