*** Settings ***
Library           Selenium2Library
Library           OperatingSystem
Library           Collections
Library           Process
Library           String
Resource          ../Test_Objects/START_PAGE.robot
Library           ../Libraries/External_Libraries.py
Library           ../Libraries/Javascript_Libraries.py
Resource          Generic.robot
Resource          TestSetup.robot

*** Keywords ***
CUSTOMER HAS VERIFIED ORGANIZATION INFORMATION
    Sleep    1s
    ${customerDb}=    get_db_details_address    ${customer}
    Sleep    1s
    ${straat}=    Execute Javascript    return ${StraatObj}.innerText
    ${huisnummer}=    Execute Javascript    return ${HuisnummeObj}.innerText
    ${postcode}=    Execute Javascript    return ${PostcodeObj}.innerText
    ${plaats}=    Execute Javascript    return ${PlaatsObj}.innerText
    ${kvk}=    Execute Javascript    return ${KVKObj}.innerText
    Sleep    1s
    Log to Console    Now comparing the screen displayed details of Customer with DB
    Highlight Object    ${StraatObj}    A
    Run Keyword If    '${straat}'=='${customerDb}[0]'    Log to Console    The Straat information is Correct
    Highlight Object    ${HuisnummeObj}    A
    Run Keyword If    '${huisnummer}'=='${customerDb}[1]'    Log to Console    The Huisnummer information is Correct
    Highlight Object    ${PostcodeObj}    A
    Run Keyword If    '${postcode}'=='${customerDb}[2]'    Log to Console    The Postcode information is Correct
    Highlight Object    ${PlaatsObj}    A
    Run Keyword If    '${plaats}'=='${customerDb}[3]'    Log to Console    The Plaats information is Correct
    Highlight Object    ${KVKObj}    A
    Run Keyword If    '${kvk}'=='${customerDb}[4]'    Log to Console    The KVK information is Correct
    Run Keyword Unless    '${straat}'=='${customerDb}[0]'    Log to Console    THE STRAAT INFORMATION IS INCORRECT
    Run Keyword Unless    '${huisnummer}'=='${customerDb}[1]'    Log to Console    THE HUISNUMMER INFORMATION IS INCORRECT
    Run Keyword Unless    '${postcode}'=='${customerDb}[2]'    Log to Console    THE POSTCODE INFORMATION IS INCORRECT
    Run Keyword Unless    '${plaats}'=='${customerDb}[3]'    Log to Console    THE PLAATS INFORMATION IS INCORRECT
    Run Keyword Unless    '${kvk}'=='${customerDb}[4]'    Log to Console    THE KVK INFORMATION IS INCORRECT

HE COMPOSE THE DETAILS FOR PREMIUM CALCULATION
    Sleep    1s
    Run Keyword If    '${ins}'=='Euler'    COMPOSE EULER PREMIUM CALCULATION
    Run Keyword If    '${ins}'=='Chubb'    COMPOSE CHUBB PREMIUM CALCULATION

COMPOSE EULER PREMIUM CALCULATION
    Sleep    1s
    Execute Javascript    ${ExpectedTurnoverObj}    #Open the first drop down
    Sleep    1s
    ${lengthOfIncomeList}=    Execute Javascript    return ${ExpectedTurnoverListObj}.length    #get the length of drpdown
    Sleep    1s
    ${lengthOfIncomeListSelected}=    Evaluate    ${lengthOfIncomeList}-2
    Sleep    1s
    ${paramJs}=    get_object_id_js    ${ExpectedTurnoverListObj}    ${lengthOfIncomeListSelected}
    ${id1}=    Execute Javascript    ${paramJs}    #get the id to select from the list
    Sleep    1s
    Execute Javascript    window.document.getElementById("${id1}").click();    #select the amount from list
    Sleep    1s
    ${turnoverValue}=    Execute Javascript    return window.document.getElementById("${id1}").innerText
    Set Global Variable    ${turnoverValue}
    Log to Console    The Expected Turnover from the dropdown has been selected and its value is ${turnoverValue}
    Execute Javascript    ${BusinessActivityObj}    #open the second dropdown
    Sleep    1s
    ${lengthofBusinessList}=    Execute Javascript    return ${BusinessActivityListObj}.length    #get the length of dropdown
    Sleep    1s
    ${lengthofBusinessList}=    Evaluate    ${lengthofBusinessList}-1
    ${paramJs}=    get_object_id_js    ${BusinessActivityListObj}    ${lengthofBusinessList}
    ${id2}=    Execute Javascript    ${paramJs}    #get the id to select from the dropdown
    Set Global Variable    ${id2}
    Sleep    1s
    Execute Javascript    window.document.getElementById("${id2}").click();    #select the value from second dropdown
    Sleep    1s
    Log to Console    The Business Activity from the dropdown has been selected

COMPOSE CHUBB PREMIUM CALCULATION
    Sleep    1s
    Input Text    id=OG_OMZET-nominal_input    ${annualTurnoverChubb}
    Set Global Variable    ${annualTurnoverChubb}
    Log to Console    The Annual Turnover for Customer entered is ${annualTurnoverChubb}
    Sleep    1s
    Execute Javascript    ${BusinessActivityObj}    #open the first dropdown
    Sleep    1s
    ${lengthofBusinessList}=    Execute Javascript    return ${BusinessActivityListObj}.length    #get the length of dropdown
    Sleep    1s
    ${lengthofBusinessList}=    Evaluate    ${lengthofBusinessList}-1
    ${paramJs}=    get_object_id_js    ${BusinessActivityListObj}    ${lengthofBusinessList}
    ${id1chubb}=    Execute Javascript    ${paramJs}    #get the id to select from the dropdown
    Set Global Variable    ${id1chubb}
    Sleep    1s
    Execute Javascript    window.document.getElementById("${id1chubb}").click();    #select the value from second dropdown
    Sleep    1s
    Log to Console    The Business Activity from the dropdown has been selected
    Sleep    1s
    Execute Javascript    ${PDMVZCREDN}
    Log to Console    The CreditCard details is selected NO
    Sleep    1s
    Execute Javascript    ${InsuredAmountObj}    #open the Second dropdown
    Sleep    1s
    ${lengthofInsuredAmtList}=    Execute Javascript    return ${InsuredAmtListObj}.length    #get the length of dropdown
    Sleep    1s
    ${lengthofInsuredAmtList}=    Evaluate    ${lengthofInsuredAmtList}-1
    ${paramJs}=    get_object_id_js    ${InsuredAmtListObj}    ${lengthofInsuredAmtList}
    ${id2chubb}=    Execute Javascript    ${paramJs}    #get the id to select from the dropdown
    Sleep    1s
    ${InsuredAmtValue}=    Execute Javascript    return window.document.getElementById("${id2chubb}").innerText
    Set Global Variable    ${InsuredAmtValue}
    Execute Javascript    window.document.getElementById("${id2chubb}").click();    #select the value from second dropdown
    Sleep    1s
    Log to Console    The Business Activity from the dropdown has been selected


ON SUBMIT PREMIUM DETAILS PAGE SHOULD BE DISPLAYED
    Sleep    1s
    Highlight Object    ${StartPageSubmitButtonObj}    StartPage.png
    Sleep    1s
    Execute Javascript    ${StartPageSubmitButtonObj}.click()    #submit
    Log to Console    The Submit button on start page has been clicked
    Wait Until Page Contains    Totale premie per maand    60s
    Log to Console    Premium Calculation Page is displayed
