*** Settings ***
Library           Selenium2Library
Library           OperatingSystem
Library           Collections
Library           Process
Library           String
Resource          ../Test_Objects/PREMIUM_CALCULATION.robot
Library           ../Libraries/External_Libraries.py
Library           ../Libraries/Javascript_Libraries.py
Library           ../Libraries/Request_Libraries.py
Resource          Generic.robot
Resource          TestSetup.robot

*** Keywords ***
CUSTOMER REVIEW THE CALCULATION
    ${premium_calc_response}=    Run Keyword If    '${ins}'=='Euler'    get_premium_calc_service_response_euler    ${customer}    ${turnoverValue}    ${id2}
    ...    ELSE IF    '${ins}'=='Chubb'    get_premium_calc_service_response_chubb    ${customer}    ${annualTurnoverChubb}    ${id2}    ${InsuredAmtValue}
    Log to Console    response received = ${premium_calc_response}
    Sleep    1s
    Highlight Object    ${GuarantorTurnoverAmtObj}    B
    ${subtotal}=    Execute Javascript    return ${SubtotalAmtObj}.innerText
    Run Keyword If    '${subtotal}'=='${premium_calc_response}[0]'    Run Keywords    Log to Console    The Subtotal is Correct
    ...    AND    Highlight Object    ${SubtotalAmtObj}    B
    Run Keyword Unless    '${subtotal}'=='${premium_calc_response}[0]'    Log to Console    The Subtotal is INCORRECT
    Sleep    1s
    ${insurancetax}=    Execute Javascript    return ${InsuranceTaxAmtObj}.innerText
    Run Keyword If    '${insurancetax}'=='${premium_calc_response}[1]'    Run Keywords    Log to Console    The Insurance Tax is Correct
    ...    AND    Highlight Object    ${InsuranceTaxAmtObj}    B
    Run Keyword Unless    '${insurancetax}'=='${premium_calc_response}[1]'    Log to Console    The Insurance Tax is INCORRECT
    Sleep    1s
    ${totalpremium}=    Execute Javascript    return ${TotalPremiumMonthAmtObj}.innerText
    Run Keyword If    '${totalpremium}'=='${premium_calc_response}[2]'    Run Keywords    Log to Console    The Total Premium is Correct
    ...    AND    Highlight Object    ${TotalPremiumMonthAmtObj}    B
    Run Keyword Unless    '${totalpremium}'=='${premium_calc_response}[2]'    Log to Console    The Total Premium is INCORRECT
    Sleep    1s
    Highlight Object    ${NextButtonObj}    B

HE SUBMIT THE PREMIUM CALCULATION
    Sleep    1s
    Execute Javascript    ${NextButtonObj}.click();
    Log to Console    The Submit button on Premium Calculation page has been clicked

ADDITIONAL INFORMATION PAGE IS DISPLAYED
    Sleep    1s
    Wait Until Page Contains    Aanvullende gegevens voor de verzekeraar    60s
    Log to Console    Additional Information Page is displayed
