*** Settings ***
Library           Selenium2Library
Library           OperatingSystem
Library           Collections
Library           Process
Library           String
Resource          ../Test_Objects/SIEBEL.robot
Library           ../Libraries/External_Libraries.py
Library           ../Libraries/Javascript_Libraries.py
Resource          Generic.robot
Resource          TestSetup.robot

*** Keywords ***
EMPLOYEE IS IN ORGANISATION SECTION
    Sleep    1s
    Highlight Object    ${OrganisatiesObj}    Click_Organisaties    # This Keyword is defined under Generic and we are passing the object to be highlighted and the name of the screenshot
    Sleep    1s
    Execute Javascript    ${OrganisatiesObj}.click();
    Log to Console    ${\n}Clicked Organisaties
    Sleep    2s
    input_keyboard_keys
    Wait Until Page Contains    Overeenkomstnummer    60s

HE SEARCH FOR THE CUSTOMER
    Sleep    1s
    Input Text    ${OrganisatieNaamObj}    ${customer}
    Sleep    1s
    Capture Page Screenshot    Enter_Customer.png
    Sleep    1s
    Highlight Object    ${ZoekenObj}    Click_Zoeken
    Execute Javascript    ${ZoekenObj}.click();
    Log to Console    Clicked zoeken after entering insurer info
    Sleep    1s
    Wait Until Page Contains    Klantbediening    60s

HE SHOULD BE ABLE TO SELECT THE CUSTOMER
    Sleep    1s
    Highlight Object    ${FoundOrganisatieNaamObj}    Found_Customer
    Sleep    1s
    Execute Javascript    ${FoundOrganisatieNaamObj}.click();
    Log to Console    Clicked Found Organisation
    Wait Until Page Contains    Besloten Vennootschap    60s

EMPLOYEE IS IN AANVRAGEN SECTION
    Sleep    1s
    Highlight Object    ${AanvraagObj}    Clicked_Aanvraag
    Sleep    1s
    Execute Javascript    ${AanvraagObj}.click();
    Log to Console    Clicked Aanvr
    Sleep    1s
    Highlight Object    ${StartAanvraagObj}    Clicked_Started_Aanvraag
    Sleep    1s
    Execute Javascript    ${StartAanvraagObj}.click();
    Log to Console    Clicked Started Aanvraag

HE NAVIGATES TO CATALOGUS ORG
    Sleep    1s
    Highlight Object    ${CatalogusORGObj}    Clicked_CatalogusORG
    Sleep    1s
    Execute Javascript    ${CatalogusORGObj}.click();
    Log to Console    Clicked CatalogusORG

HE SHOULD BE ABLE TO SELECT THE INSURANCE
    Sleep    1s
    Highlight Object    ${VerzekerenObj}    Verzekeren
    Sleep    1s
    Execute Javascript    ${VerzekerenObj}.click();
    Log to Console    Clicked Verzekeren

HE SELECTS THE INSURANCE TO CREATE ORDER
    Sleep    1s
    Log to Console    Looking for Insurance = ${insurance} Position on the List
    ${count}=    Execute Javascript    return ${LengthofVerzekerenObj}.length
    Log to Console    Count = ${count}
    : FOR    ${i}    IN RANGE    1    ${count}
    \    ${param_js}=    get_element_of_commercial_verzekering    ${i}
    \    ${Val}=    Execute Javascript    ${param_js}
    \    Sleep    1s
    \    Exit For Loop If    '${Val}'=='${insurance}'
    Log To Console    Iteration found in = ${i}
    Set Global Variable    ${i}
    Sleep    1s
    Execute Javascript    ${KanaalObj}.click();
    Sleep    1s
    Log to Console    Clicked Kanaal to open drop down list
    Execute Javascript    ${Face2FaceObj}.click()
    Sleep    1s
    ${value}=    Execute Javascript    return ${Face2FaceObj}.innerText
    Log to Console    value of dropdown= ${value}
    Execute Javascript    ${CommentLabelObj}.click();
    Sleep    2s
    Log to Console    Selected Face2Face
    Capture Page Screenshot    Face2Face.png
    ${InsuranceObj}=    get_element_of_insurance_obj    ${i}
    Highlight Object    ${InsuranceObj}    Clicked_Insurance
    Sleep    5s
    Run Keyword And Continue On Failure    Execute Javascript    ${InsuranceObj}.click();
    Sleep    1s
    ${present}=    Run Keyword And Return Status    Page Should Not Contain    Kanaal een waarde ingevuld te worden
    Sleep    1s
    Log To Console    ${present}
    Log To Console    ${i}
    Comment    Run Keyword If    ${present}    Click Element    //a[contains(.,'Ok')]
    Run Keyword If    '${browser}'=='ie'    Execute Javascript    ${InsuranceObj}.click();
    Log to Console    Insurance Product Selected

HE CLICK ON SUBMIT BUTTON
    Sleep    1s
    Highlight Object    ${OrderObj}    Selcted_Product
    Sleep    1s
    Run Keywords    Execute Javascript    ${OrderObj}.click();
    ...    AND    input_keyboard_keys
    Log To Console    Application Submitted
    Sleep    2s

THE START PAGE SHOULD BE DISPLAYED
    Sleep    2s
    ${excludes}=    Get Window Handles
    Log To Console    Escludes = ${excludes}
    Switch Window    ${excludes}[1]
    ${url1}=    Get Location
    Log To Console    urls = ${url1}
    Sleep    1s
    Add Cookie    x-rabo-employee    3033Allroles,7000008307
    Sleep    1s
    Go To    ${url1}
    Sleep    2s
    ${present}=    Run Keyword And Return Status    Wait Until Page Contains    Premie berekenen en aanvragen    60s
    Log To Console    Is Present ? ${present}
    ${customerName}=    Execute Javascript    return ${CustomerNameObj}.innerText
    Run Keyword If    '${customerName}'=='${customer}'    Log To Console    The Displayed Customer Name is Correct
    Highlight Object    ${CustomerNameObj}    CustomerName
