*** Settings ***
Resource          E2E_KEYWORDS.robot

*** Keywords ***
Employee has already selected the customer
    GIVEN EMPLOYEE IS IN ORGANISATION SECTION
    WHEN HE SEARCH FOR THE CUSTOMER
    THEN HE SHOULD BE ABLE TO SELECT THE CUSTOMER

Employee is in Aanvraagregels section
    Employee has already selected the customer
    GIVEN EMPLOYEE IS IN AANVRAGEN SECTION
    WHEN HE NAVIGATE STO CATALOGUS ORG
    THEN HE SHOULD BE ABLE TO SELECT THE INSURANCE

Customer is in Start Page
    Employee is in Aanvraagregels section
    GIVEN HE SELECTS THE INSURANCE TO CREATE ORDER
    WHEN HE CLICK ON SUBMIT BUTTON
    THEN THE START PAGE SHOULD BE DISPLAYED

Customer is in Premium Calculation Page
    ${Suite_Status}=    containString    ${SUITE NAME}    LOCALBANK
    Log To Console    Suite Status = ${Suite_Status}
    Set Global Variable    ${Suite_Status}
    Run Keyword If    '${Suite_Status}'=='True'    Customer is in Start Page
    GIVEN CUSTOMER HAS VERIFIED ORGANIZATION INFORMATION
    WHEN HE COMPOSE THE DETAILS FOR PREMIUM CALCULATION
    THEN ON SUBMIT PREMIUM DETAILS PAGE SHOULD BE DISPLAYED

Customer is in Additional Information Page
    Customer is in Premium Calculation Page
    GIVEN CUSTOMER REVIEW THE CALCULATION
    WHEN HE SUBMIT THE PREMIUM CALCULATION
    THEN ADDITIONAL INFORMATION PAGE IS DISPLAYED
