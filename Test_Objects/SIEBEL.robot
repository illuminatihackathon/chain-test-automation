*** Variables ***
${OrganisatiesObj}    window.document.getElementsByClassName("ui-tabs-anchor")[3]
${OrganisatieNaamObj}    //*[@id="a_3"]/div/table/tbody/tr[9]/td[2]/div/input
${ZoekenObj}      window.document.getElementById("s_3_1_31_0_Ctrl")
${FoundOrganisatieNaamObj}    window.document.getElementsByName("ShortName")[0]
${AanvraagObj}    window.document.getElementsByClassName("ui-tabs-anchor")[30]
${StartAanvraagObj}    window.document.getElementById("a_4").querySelector("div").querySelector("button")
${CatalogusORGObj}    window.document.getElementsByClassName("ui-tabs-anchor")[19]
${VerzekerenObj}    window.document.getElementById("s_6_l").querySelectorAll("tr")[13].querySelectorAll("td")[1]
${LengthofVerzekerenObj}    window.document.getElementById("s_5_l").querySelectorAll("tr")
${KanaalObj}      window.document.getElementsByClassName("mceGridField siebui-value mceField")[11].querySelector("span")
${Face2FaceObj}    window.document.getElementsByClassName("ui-menu ui-widget ui-widget-content ui-autocomplete ui-front")[0].querySelectorAll("div")[3]
${OrderObj}       window.document.getElementsByClassName("siebui-btn-grp-search")[8].querySelectorAll("button")[0]
${CommentLabelObj}    window.document.getElementsByTagName("textarea")[1]
${CustomerNameObj}    window.document.getElementById("OG_ANAAM")
