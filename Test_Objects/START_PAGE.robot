*** Variables ***
${StraatObj}    window.document.getElementById("OG_VSTRAAT")
${HuisnummeObj}    window.document.getElementById("OG_VHUISNR")
${PostcodeObj}    window.document.getElementById("OG_PSTCD")
${PlaatsObj}    window.document.getElementById("OG_VPLTS")
${KVKObj}    window.document.getElementById("OG_NRHNDRG")
${ExpectedTurnoverObj}    window.document.getElementById("KR_OMZAKK-button-label").click();
${ExpectedTurnoverListObj}    window.document.getElementsByClassName("rfs-select__option")
${BusinessActivityObj}    window.document.getElementsByClassName("rfs-searchable-select__button rfs-hidden-xs-down")[0].click();
${BusinessActivityListObj}    window.document.getElementsByClassName("rfs-searchable-select__options")[0].querySelectorAll("li")
${StartPageSubmitButtonObj}    window.document.getElementsByClassName("rfs2-button rfs2-button--primary")[0]
${PDMVZCREDN}    window.document.getElementById("PD_MVZCRED-N").click();
${InsuredAmountObj}    window.document.getElementsByClassName("rfs-select__button rfs-hidden-xs-down")[0].click();
${InsuredAmtListObj}    window.document.getElementsByClassName("rfs-select__option")
